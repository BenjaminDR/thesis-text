\chapter{Background}
\label{chap:background}

% TODO: background introduction

\section{Intel SGX and Trusted Execution Environments}

A \gls{tee} is a processing environment where applications can be executed in isolation from the rest of the system \cite{TEEs-paper}.
It provides some level of assurance in the integrity of code and data as well as the confidentiality of that data.
If these assurances hold, it is guaranteed that we can trust data to remain confidential inside a \gls{tee} and that the intended operations are performed on that data \cite{CCC-whitepaper}.
\glspl{tee} can be hardware-enforced, meaning that hardware is responsible for protecting the assurances of this environment.
This allows the isolation of a \gls{tee} embedded application to be extended to highly privileged software components like the operating system or a hypervisor.
Often, some kind of attestation is present where a \gls{tee} provides cryptographic evidence of its state and origin to establish trust with an outside party.

In 2015, Intel made the \glsfirst{sgx} public as an extension to the x86 computer architecture.
Intel \gls{sgx} is a hardware-enforced \gls{tee} that isolates applications in so-called ``enclaves''.
To do so, a fixed region in the shared address space is reserved for enclaves.
During operation, the processor can programatically switch between enclave mode and non-enclave mode.
Only when in enclave mode, memory accesses to enclave memory will succeed.
Enclave memory is encrypted to protect against known hardware attacks \cite{sgx-sdk-developer-reference-linux}.

In the remainder of this section, some relevant topics regarding Intel \gls{sgx} will be briefly covered.
All technical details were derived from \cite{intel-architecture-manual, sgx-explained}.

\subsection{SGX structures}
Intel \gls{sgx} introduces a number of control and data structures that are tied to its operation.
Due to their relevance in context switching to and from the enclave, two structures will be briefly mentioned.

To fully benefit from multi-core processors, \gls{sgx} is designed with multi-threading in mind.
An Intel \gls{sgx} enclave can be build with multiple \glspl{tcs}.
Each concurrent thread executing in the enclave is associated to a separate \gls{tcs}.
When a logical processor enters enclave mode, the associated \gls{tcs} is marked as busy.
No other logical processor will be allowed to simultaneously use the same \gls{tcs} to enter the enclave.
After the thread exits the enclave, the \gls{tcs} is marked as available again.

To each \gls{tcs} a stack of one or more \gls{ssa} frames is linked.
An \gls{ssa} frame is a memory area that is used to store the execution state of a logical processor.
This is to allow asynchronous context switches between the enclave and the outside untrusted world, meaning context switches that are not software controlled but both initiated and executed by hardware in response to an asynchronous event.

\subsection{Entering and exiting enclaves}
Two new instructions can be used to transition a logical processor to and from enclave mode.
The new EENTER instruction will put the logical processor into enclave mode and branch to a specific entry point that is stored in the \gls{tcs}.
Only when the processor is inside enclave mode, the EEXIT instruction can be used to branch outside of the enclave and leave enclave mode.
These events are referred to as a synchronous enclave entry and synchronous enclave exit.

In the event of a hardware exception (e.g. a fault or interrupt) while the processor is executing in enclave mode, execution control needs to be transferred back to untrusted host to handle the exception.
This event is referred to as an \gls{aex}.
In the event of an \gls{aex}, hardware will automatically store the current code execution context in the current \gls{ssa} frame, leave enclave mode and branch to an exception handler on the untrusted host.
Later, the untrusted host can resume execution in the enclave by issuing the new ERESUME instruction.
This instruction switches the logical processor back to enclave mode and restores the execution context that was saved in the \gls{ssa} frame. 

A problem occurs when the hardware exception is directly caused from inside the enclave.
If so, the exception handler on the untrusted host is not able to mitigate the issue.
If enclave execution is then simply resumed with the ERESUME instruction, the same instruction will again trigger the hardware exception and initiate the \gls{aex} procedure.
This has rendered the enclave effectively useless.
To mitigate this issue, \gls{sgx} provides an alternative method to enter an interrupted enclave.
If there is still an unused \gls{ssa} frame available for that thread, the previously interrupted enclave can be entered again with the EENTER instruction.
This entry is now associated with a different \gls{ssa} frame.
Enclave software can now execute in an execution context that is entirely separate from the one stored in the first \gls{ssa} frame on \gls{aex}.
This means, enclave software can try to mitigate the issue that caused the \gls{aex} and leave the enclave again as to resume its first execution context.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/SGX-entry-exit-events.png}
    \caption{Enclave entry and exit events for a \gls{tcs} with two \gls{ssa} frames. CSSA is an index for the current \gls{ssa} frame that is stored in the \gls{tcs}. Figure adopted from \protect\cite{sgx-explained}.}
    \label{fig:sgx-entry-exit-events}
\end{figure}
This process is visualized in figure \ref{fig:sgx-entry-exit-events}.

Two operands need to be given to the EENTER instruction when it is executed.
This can be done by loading the operands in specific registers.
A first operand points to the \gls{tcs} that will be used to enter the enclave.
\gls{sgx} hardware will check if this \gls{tcs} is available for entry, calculate the entry point from the information in the \gls{tcs} to know the target branch location.
A second parameter provides the hardware with an address that can be jumped to in case of an \gls{aex}.

The EEXIT instruction only receives one operand.
This is the address of the branch target location for the synchronous enclave exit procedure.
Contrasting to enclave entry, the branch target on enclave exit is therefore not fixed when the enclave is created but instead configurable at runtime.



\section{Intel SGX software ecosystem}

While Intel \gls{sgx} ensures memory isolation at the hardware level, it is up to the application developer to propagate the \gls{sgx} security properties to the software level.
This has proven to be a particularly elaborate task.
All software that is not part of the enclave needs to be seen as inherently untrusted.
It needs to be assumed that this software can execute payloads that are specifically designed to adversely influence trusted software.
Under \gls{sgx}, the entire untrusted memory region and architectural \gls{cpu} state is shared between both untrusted and trusted software.
While this is useful to provide any interaction between host and enclave, it simultaneously provides the means for a wide range of attack vectors at the software level.

There is also a need for more functional abstractions at the software level.
From a functional perspective, the EENTER and EEXIT instructions can be seen as constrained jump instructions.
When working in higher-level programming languages, a more rich function-like abstraction between host and enclave may be desired.
Building such an abstraction is not trivial as it is not safe to share an execution stack between the enclave and the untrusted host.

The vast intricacies of using the \gls{sgx} technology in a both functional and safe manner require a substantial amount of specialized knowledge.
To solve this issue, software runtimes are designed by experts to implement useful functional software abstractions for applications.
These runtimes aim to provide a useful software interface that can be used to both shield the application from malicious tampering of untrusted software; and shield the application developer from complicated intricacies that surround the Intel \gls{sgx} technology.

Over time, a vast variety of enclave shielding runtimes have emerged that provide different levels of transparency to enclave developers.
There exists full-fledged \glspl{sdk} or frameworks like the Intel \gls{sgx} \gls{sdk} \cite{intel-sgx-sdk, intel-sdk-github}, Microsoft's Open Enclave \gls{sdk} \cite{oe-sdk, oe-sdk-github} or the Google Asylo framework \cite{google-asylo}.
The Fortanix Rust \gls{edp} \cite{rust-sdk, rust-edp-github} is partially embedded in the standard Rust compiler \cite{rust-lang-github} to allow interaction with their enclave development tools at the level of the standard library.
Graphene \cite{graphene} and Microsoft's SGX-LKL \cite{sgx-lkl} are examples of shielding runtimes that move their interface to the Intel \gls{sgx} technology inside a library OS at the syscall level, allowing applications to run unmodified inside an \gls{sgx} enclave.

While the \gls{sgx} software ecosystem matures, these enclave shielding runtimes allow \gls{sgx} technology to be used in applications ranging from confidential database software \cite{edgeless-db} to privacy-preserving contact discovery in the Signal messenger service \cite{signal-sgx}.

%\section{ABI shielding research}

% TODO: ABI shielding research
