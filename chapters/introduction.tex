\glsresetall

\chapter{Introduction}
In a highly digitalized and interconnected world, the ever growing need for secure software components becomes increasingly apparent.
Universally, critical infrastructure imposes strong integrity requirements on software systems.
Meanwhile, the abundance of secrets that are stored in the digital world generate immense economical and political incentives to breach software and data confidentiality.
Accordingly, infringement of software security requirements poses risks to both industry and society.
Next to an overall growth in importance, we observe a simultaneous surge in the complexity of today's software systems.
As more powerful abstractions are made, this complexity surge has likewise become apparent in more privileged software- and hardware-layers like operating systems, hypervisors and \gls{cpu} microcode.
Considering that common software applications rely on the integrity of these layers, it is ever more detrimental how increasing software complexity gives rise to new; and more widespread, attack vectors.
This has provoked a search for stronger isolation strategies to further shield software applications from the vast amount of complexity that it depends on.
One such technology, that is now widely deployed on commodity systems that support the x86 architecture, are the Intel \gls{sgx} \cite{intel-sgx}.
With its uniquely strong attacker model and wide availability, Intel \gls{sgx} enjoyed substantial attention from both industry and academia.
Its ambitious security properties aim to protect software applications from powerful adversaries such as as the operating system it is running on.
A large research effort continues to uncover the intricacies that arise from this powerful attacker model.
Various lines of research have been explored regarding side-channel attacks \cite{vanbulck2018nemesis, high-res-side-channels, vanbulck2017sgxstep, Murdock2019plundervolt, VoltPillager}, transient execution attacks \cite{vanbulck2018foreshadow, SgxPectre, Schwarz2019ZombieLoad, vanbulck2020lvi} and memory corruption \cite{lee_hacking_darkness, biondo_guards_dilemma, askoxylakis_asyncshock_2016}.

\gls{sgx} isolates applications in so-called ``enclaves'', memory access to these enclaves is strictly regulated by the \gls{cpu}.
All memory outside the enclave can potentially be malicious and is therefore inherently untrusted.
A more recent line of research \cite{vanbulck2019tale, alder_faulty_2020, khandaker_coin_2020, cloosters_teerex} focuses on the attack landscape that is exposed by the enclave interface.

\section{Enclave interface}

When code outside the enclave is executing, enclave memory is strictly inaccessible.
Only when the processor switches to an ``enclave mode'', execution branches to trusted code inside the enclave.
While in this enclave mode, access to enclave memory is permitted by hardware.
Switching to and from enclave mode is done through a very limited hardware interface of only a few specialized instructions.
Surrounding these few instructions, a stronger software software abstraction can be built to provide interaction between the trusted enclave and the untrusted host.
This software layer is highly critical as it takes part in shielding the enclave from the untrusted host.

Over time, numerous enclave shielding runtimes are created with varying use cases and target programming languages.
These runtimes both shield a developer from the technicalities of the \gls{sgx} technology, as shield a trusted application from malicious interaction with the untrusted host.
Van Bulck et al. \cite{vanbulck2019tale} were the first to uncover a wide vulnerability landscape in these emerging shielding runtimes.
Runtime weaknesses can be categorized in an \gls{abi}-tier and \gls{api}-tier.
The \gls{abi}-tier considers interaction with low-level architectural state that is generally hidden to high-level programming languages.
This includes the abundance of registers present in the x86 architecture as well as interaction with the execution stack.
The \gls{api}-tier considers assumptions made by a program at a higher level.
This involves the safe use of concepts like pointers, strings or composite objects.

This work focuses on the potential of an attacker inducing misbehavior at the \gls{abi} level.
Each enclave shielding runtime is required to implement some kind of \gls{abi} layer written in low-level assembly code to sanitize low-level state on enclave entry, change execution stacks and clear any secrets from registers before exiting the enclave.
After initial work by Van Bulck et al. \cite{vanbulck2019tale}, more \gls{abi}-level attack vectors have recently been uncovered by Alder et al. \cite{alder_faulty_2020}.
These vulnerabilities where not tied to one single runtime, but where observable in a wide range of studied enclave shielding runtimes.
This highlights the intricacies in \gls{abi} shielding responsibilities when interacting with highly privileged adversaries.
Still, while the amount of possible pitfalls grows, we see runtimes creating and maintaining separate \gls{abi} layer implementations that aim to achieve very similar goals.
This has prompted the following research question.\\
\textit{Is it feasible to engineer a secure unified \gls{abi} sanitization layer that can be shared across different shielding runtimes for Intel \gls{sgx} enclaves?}


\section{Contributions}

This work focuses on the general \gls{abi} related responsibilities that software needs to fulfill to fully protect Intel \gls{sgx} enclaves. 
The inner workings of existing \gls{abi} layers are studied while possibilities for their unification are explored.
More specifically, the following contributions are made.

\begin{itemize}
    \item A specialized \gls{abi} is needed between a binary running on an untrusted host and inside a trusted Intel \gls{sgx} enclave. We carefully analyze and summarize the existing measures that need to be taken to make such a specialized \gls{abi} functional while preserving the strong security requirements of Intel \gls{sgx}.

    \item By manual code analysis, we unravel how the Intel \gls{sdk} \cite{intel-sgx-sdk, intel-sdk-github} and Fortanix Rust \gls{edp} \cite{fortanix-web} runtimes build a functional \gls{abi} between a binary running on the untrusted host and embedded in an \gls{sgx} enclave. We document different approaches that are taken by both runtimes and how they relate to their respective needs.

    \item We fully analyze the low-level enclave entry- and exit-code that is used for the Intel \gls{sdk} and Fortanix Rust \gls{edp} runtimes. We create a multi-level dissection of how these assembly stubs operate and identify all possible execution paths.

    \item We compose a design philosophy to guide the engineering process of a unified \gls{abi} sanitization layer. In addition a series of best code practices are established based on past vulnerabilities that were found in the trusted \gls{abi} layer.

    \item We show that it is possible to build an \gls{abi} sanitization layer that supports multiple enclave shielding runtimes with multiple target programming languages by creating a working prototype. We show the feasibility of using such a unified \gls{abi} by successfully embedding the prototype into the Intel \gls{sdk} and Fortanix Rust \gls{edp} shielding runtimes without significant adaptation. This implementation is evaluated based on the design philosophy and code practices that were previously defined.
\end{itemize}

The source code of the unified \gls{abi} layer prototype is open sourced to aid future research.
The repository is publicly available at \url{https://gitlab.com/BenjaminDR/unified-abi-sanitization-layer}.

\section{Outline}

The remainder of this text is structured as follows.

\medskip
\noindent
\textbf{Chapter \ref{chap:background}: \nameref{chap:background}}
The background chapter introduces the technical concepts that will be relevant for the remainder of this master's thesis. More information is given on the inner workings of the Intel \gls{sgx} technology and the role of enclave shielding runtimes in the \gls{sgx} software ecosystem.

\medskip
\noindent
\textbf{Chapter \ref{chap:ABI-analysis}: \nameref{chap:ABI-analysis}}
This chapter highlights various problems and design decisions that are inherent to the creation of an \gls{abi} layer for enclave shielding runtimes.
It does so by analyzing the \gls{abi} layer implementation of two enclave shielding runtimes, the Intel \gls{sgx} \gls{sdk} and Fortanix Rust \gls{edp}.

\medskip
\noindent
\textbf{Chapter \ref{chap:unification}: \nameref{chap:unification}}
This chapter explores the feasibility of a unified \gls{abi} layer implementation for enclave shielding runtimes.
The engineering process of an \gls{abi} layer implementation that can shield both the Intel \gls{sdk} and Rust \gls{edp} runtimes is discussed.
To reason about its security and usage in a unified setting, a series of functional requirements and security related design decisions are defined to evaluate the working prototype.

\medskip
\noindent
\textbf{Chapter \ref{chap:conclusion}: \nameref{chap:conclusion}}
Finally, we conclude our contributions in this chapter.
Limitations and challenges that are inherent to this work are highlighted.
Lastly, we focus on possible lines of research that can expand on the findings in this master's thesis.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "thesis"
%%% End: 
