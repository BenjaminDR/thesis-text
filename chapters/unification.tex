\chapter{Unification of the ABI layer}
\label{chap:unification}

\section*{Preamble}

The previous chapter went into great detail about how two very different enclave shielding runtimes have built an \gls{abi} sanitization layer.
This chapter returns back to our original research question by looking at the feasibility of engineering a unified implementation that can secure enclave shielding runtimes.
Before one can reason about the feasibility of using such a unified \gls{abi} layer, first it needs to be established that creating a unified implementation is at all possible.
One of our contributions, presented in this chapter, will confirm this is the case by introducing a functional prototype of a trusted \gls{abi} sanitization layer.
Building from this prototype, we can continue to reason about its security and feasibility to embed in existing enclave shielding runtimes.

This chapter will start by establishing functional requirements that can steer the design process of a new \gls{abi} layer towards a more flexible implementation that is able to satisfy the needs of more than one trusted runtime.
It will consecutively also form a security oriented design philosophy to apply the knowledge gained from previous enclave \gls{abi} research \cite{vanbulck2019tale, alder_faulty_2020} in a principled way during the design phase.
Before diving into all design decisions and relevant implementational details in \S \ref{sec:design-and-implementation}, current limitations of the prototype are described in \S \ref{sec:prototype-limitations}.
Finally, \S \ref{sec:prototype-evaluation} evaluates the prototype against the functional and security oriented goals we established.

\section{Requirements and design philosophy}
\label{sec:requirements-design-philosophy}

The prototype will function as a replacement for trusted \gls{abi} layer implementations across enclave shielding runtimes. 
No separate untrusted \gls{abi} layer will be created, therefore the prototype will interact with the runtime specific untrusted \gls{abi} layers.
Although unifying this related untrusted component and integrating it in the prototype would be useful in completely concealing \gls{abi} complexity from enclave shielding runtimes, it would not bring any benefits concerning the security of enclave shielding runtimes.
We therefore lay focus solely on the trusted \gls{abi} layer residing in the enclave.

\subsection{Functional requirements for unification}
\label{sec:functional-requirements-unification}

Prior to designing a unified \gls{abi} sanitization layer, we first formulate a series of functional requirements that should be satisfied in the final prototype. These requirements are specifically formed to steer the design process of the prototype in such a way that makes it feasible to secure a variety of runtimes.

\begin{enumerate}
    \item The prototype should adhere to the System V \cite{System-V-ABI} calling convention. Any code that also adheres to this convention should be able to use our edge calling interface.
    \item The \gls{abi} and \gls{api} layer should be clearly separated. We therefore require the trusted \gls{abi} layer to use variables that are not shared with the \gls{api} layer and to maintain the consistency of these variables without help from the \gls{api} layer. To do so, the \gls{abi} has to check if its interface is correctly used (e.g. no \gls{oret} should be triggered if there was no previous \gls{ocall} that needs to be returned to). This frees the \gls{api} from this responsibility and allows it to be oblivious to the edge calling order.
    \item The prototype can be integrated into existing runtimes without major code changes. The studied enclave shielding runtimes should be able to incorporate our prototype to shield their trusted runtime by applying a limited patch (minor code reorganization, substitution of function targets). The prototype should therefore be able to handle different enclave memory layouts.
    \item The prototype should be configurable to exclude potentially unwanted features. Enclave shielding runtimes that do not need all features are therefore not forced to incorporate dead code, meaning code that will never get executed, in their trusted runtime.
    \item The unification process does not counter simplicity by introducing runtime specific code.
\end{enumerate}

It is our belief that a well-engineered, useful unified \gls{abi} layer should be able to satisfy these requirements.

\subsection{Security oriented design principles}
\label{sec:security-oriented-design-principles}

Motivated by the susceptibility of existing \gls{abi} layers to security vulnerabilities \cite{vanbulck2019tale, alder_faulty_2020, oe-eresolve-vuln}, we aim to design this \gls{abi} layer to be robust against all known \textit{and} possible future attacks.
We do this by adhering to the following principles when possible.

\subsubsection{Sanitize/cleanse eagerly} 
The trusted \gls{abi} layer should be eager in the sanitization of architectural state components on entry.
This reduces the amount of attacker controlled values that enter the enclave and could potentially maliciously affect it.
This would harden the \gls{abi} to possible future attacks involving unsanitized state.
Similarly, the \gls{abi} layer should be eager in cleansing state components before enclave exit to remove potential enclave secrets or other information that can (indirectly) benefit an adversary.

\subsubsection{Sanitize early, cleanse late} 
To maximally reduce the scope of enclave execution, where part of the architectural state is attacker controlled, state components should be sanitized as early as possible. 
Cleansing these state components as late as possible, i.e. close to the enclave exit point, reduces the risk of runtime information being left accessible to the untrusted host in these registers.

\subsubsection{Minimize cyclomatic complexity} 
Cyclomatic complexity \cite{mccabe1983structured} is a metric that quantifies the amount of linearly independent execution paths in a computer program. Limiting the amount of linearly independent execution paths, simplifies accounting for possibly erroneous edge cases.

\subsubsection{Minimize functional complexity} 
Any functionality that can analogously be implemented in an untrusted setting, should be moved outside of the trusted \gls{abi} layer, and be implemented in the untrusted \gls{abi} layer instead. 
Functionality that needs a trusted setting but can be implemented outside the \gls{abi} layer, should not be included in the trusted \gls{abi} layer. 
Instead it can be implemented in a less error prone higher level language where it can rely on a trusted \gls{abi}.

\subsubsection{Avoid mutable \gls{eresolve} dependencies} 
The design and attacker model of Intel \gls{sgx} allows the untrusted host to interrupt an enclave at any given time.
With a framework like SGX-Step \cite{vanbulck2017sgxstep}, this can be done at an instruction level granularity. 
This means that it is in the power of an attacker to trigger the \gls{eresolve} operation in a calculated manner at any point during enclave execution. 
Determining all possible \gls{eresolve} execution paths becomes increasingly difficult when it depends on mutable objects. 
The \gls{abi} designer now needs to evaluate their implementation across every possible state transition of these mutable dependencies throughout enclave execution. 
Although technically different, this is comparable to the complexity of two different threads operating on a shared dependency in a concurrent setting. 
Recently an elevation of privilege vulnerability \cite{oe-eresolve-vuln} was found in the Open Enclave trusted runtime \gls{abi} \cite{oe-sdk, oe-sdk-github} because an attacker could interrupt the enclave before an \gls{eresolve} dependency was correctly initialized.

\subsection{Trade offs}

Our research on the Intel \gls{sdk} in chapter \ref{chap:ABI-analysis} has revealed that it follows the \textit{Minimize functional complexity} principle very closely.
Logic implemented in assembly is kept to a strict minimum with the direct consequence that all checks safeguarding the correct usage of the edge-calling interface are implemented outside of the \gls{abi} layer in the higher level C++ language.
Doing so results in a tight integration of the lower level \gls{abi} code and the higher level \gls{api} code.
As previously mentioned in Section \ref{sec:functional-requirements-unification}, we argue that for a \textit{unified} \gls{abi} layer, a clear separation between the \gls{abi} and \gls{api} layers is preferred.
By requiring the \gls{abi} layer to defensively check correct usage of it's interface, this responsibility is not shifted back to the runtimes that would use this unified layer.
It arguably also makes sense from a software engineering standpoint that library consistency is conserved within the library, in favor of doing so before each invocation of its interface.
Because of these reasons, we chose to deviate from the \textit{Minimize functional/cyclomatic complexity} principles for this specific case.
The additional complexity introduced by these checks is minimal with 10 additional instructions to safeguard the \gls{oret} operation and 6 additional instructions to safeguard the \gls{ocall} operation.

\section{Prototype limitations}
\label{sec:prototype-limitations}

\subsection{Untrusted \gls{abi} layer}
The prototype does not include an untrusted \gls{abi} layer.
The absence of a corresponding untrusted \gls{abi} layer makes integration of the prototype more dubious as low level assembly code will still need to be written to pass parameters in the correct registers and ensure compliance with the System V calling convention on the untrusted side.

\subsection{XSAVE availability}
Equivalently to the sanitization procedure of the Rust \gls{edp} for extended feature state (cf. \S \ref{sec:abi-tier-sanitization-ext-feature-state}), it is assumed in this prototype that the XSAVE feature set is available.
The x86\_64 architecture is however still backwards compatible with systems that do not support this extension.
To these systems, the current prototype would not be usable as the enclave would exit with an exception on each entry.

\subsection{Untrusted stack allocation}
\label{sec:limitations-untrusted-stack-allocation}
The Intel \gls{sdk} supports a way to temporarily allocate a buffer in the red zone of the untrusted stack by exiting the enclave with an adjusted untrusted stack pointer.
The Intel \gls{sdk} supports a way to temporarily allocate a buffer in the red zone of the untrusted stack (cf. \S \ref{sec:untrusted-stack-allocation}) by exiting the enclave with an adjusted untrusted stack pointer.
Although we recognize the benefits of this feature in the function call abstraction that the Intel \gls{sdk} has built, the general need for this feature is not immediately clear to us.
Presently, the untrusted stack pointer is not altered in any way by the unified \gls{asl}.
On enclave exit, the untrusted stack pointer will be set to its value on the previous \gls{ecall} in the case of an \gls{ocall} or \gls{eret}; or its value on the previous \gls{eresolve} in case of an \gls{eresret}.


\section{Design and implementation}
\label{sec:design-and-implementation}

\subsection{Edge calling interface}
\label{sec:unified-edge-calling-interface}

\begin{figure}[ht]
    \centering
    \includegraphics[]{figures/Unified ASL edge calling interface.png}
    \caption{Overview of the edge calling interface of the unified \gls{abi} sanitization layer. An arrow facing to the \gls{abi} layer indicates the initiation of the operation. An arrow facing away from the \gls{abi} layer indicates that the \gls{abi} layer jumps to an end point after execution of an edge call operation. Solid arrows represent function calls, dashed lines represent function returns.}
    \label{fig:unified-asl-edge-calling-interface}
\end{figure}

The trusted edge calling interface for this prototype is visualized in figure \ref{fig:unified-asl-edge-calling-interface}. 
It is similar to the interface of the Intel \gls{sdk} because the same edge call operations are supported.
However, the Intel \gls{sdk} jumps to the same function for each differentiated entry situation. 
Later it differentiates between these situations again outside of the \gls{abi} layer in higher-level C++ code.
The unified \gls{asl} is designed to keep different entry situations explicitly separated.
The \gls{abi} layer will jump to different functions for \textit{root} \glspl{ecall}, nested \glspl{ecall} and \gls{eresolve} entries. A root \gls{ecall} is defined as an \gls{ecall} without a previous trusted execution context, i.e. without a previous \gls{ocall} still waiting on a corresponding \gls{oret} operation.

Much like the Intel \gls{sdk}, the unified \gls{asl} allows for more complex ``call or return'' policies to be defined in a higher-level language outside of the \gls{abi} layer.
This has the advantage that any runtime using this \gls{abi} layer can have an additional say in the situations where an \gls{oret} or nested \gls{ecall} is permitted.
This is implemented similarly by ignoring any argument originating from the untrusted side that would indicate that their intention is to do an \gls{ecall} or \gls{oret} and simply jumping to the target function for a nested \gls{ecall}.
Here, a policy can decide to trigger the \gls{oret} operation, which will rollback the trusted stack by removing the \gls{ecall} frame and subsequently return from the last \gls{ocall}.
If the policy decides to treat the entry as a nested \gls{ecall}, it can simply call other functions with the current trusted stack layout.
Initiating the \gls{oret} operation when the enclave was entered by a root \gls{ecall} or \gls{eresolve} is not permitted.
For this prototype we chose to explicitly disable this option from within the \gls{abi} layer by doing two extra checks and aborting the operation by returning an error if necessary.

Control flow bookkeeping is done by saving a pointer to the last \gls{ocall} frame in the \gls{tls}. 
Each \gls{ocall} frame will link to the previous one, forming a linked-list like structure.

Figure \ref{fig:unified-asl-entry-flowchart} shows a summarized view of the entry code in the unified \gls{asl}.
This flowchart shows that the execution paths of different entry situations are kept separate.
The runtime does not need to distinguish the type of entry situation again because each execution path branches to a different higher level function.

\begin{figure}[p]
    \centering
    \includegraphics[height=0.92\textheight]{figures/Unified ASL entry - level 1.png}
    \caption{Overview of the enclave entry code inside the trusted \gls{abi} sanitization layer prototype. Some steps are omitted for brevity.} 
    \label{fig:unified-asl-entry-flowchart}
\end{figure}

\subsection{Calling convention}
\label{sec:unified-calling-convention}

Parallel to studied runtimes, the unified \gls{asl} supports parameter passing of a fixed number of \textit{INTEGER}-class parameters.
For function calls, we fixed the number of parameters originating from the runtime to six with potential supplementary parameters originating from hardware.
For function returns, the number of parameters is fixed to two return values.
Similarly to the studied runtimes, the unified layer will not fetch parameters from untrusted memory.

Register preservation is conform with the System V \gls{abi} calling convention \cite{System-V-ABI} on the trusted side.
We share the view of the Intel \gls{sdk} that untrusted callee-save state is better stored and restored in the untrusted \gls{abi} layer.
This results in less logic in the, more security critical, trusted \gls{abi} layer.

\subsection{Construction and initialization dependencies}
\label{sec:construction-initialization-dependencies}

The unified \gls{asl} is designed to have minimal dependencies on how an enclave is constructed and initialized.
Because of this flexibility, enclave shielding runtimes remain in control of how the enclave is built and when it is initialized.

\subsubsection{Trusted stacks}
An unavoidable dependency is the presence of an enclave memory region to construct the main trusted execution stack.The unified \gls{asl} does, however, not depend on a specific placement of this trusted stack in enclave memory.
Instead, the specific offset, relative to the placement of the \gls{tcs} is configurable.

If exception handling is enabled in the unified \gls{asl}, an extra enclaved memory region is required to construct a separate trusted exception stack.
Similarly to the main trusted stack, it's relative location is configurable.
The Open Enclave shielding runtime \cite{oe-sdk, oe-sdk-github} has shown that it is possible to remove this dependency and construct the trusted exception stack in the same stack region, on top of the main trusted stack.
This comes with the penalty of deviating from the \textit{Minimize cyclomatic complexity}, \textit{Minimize functional complexity} and \textit{Avoid mutable \gls{eresolve} dependencies} principles defined in Section \ref{sec:security-oriented-design-principles}.
The implementation of Open Enclave of constructing this exception stack on top of the main trusted stack recently suffered from a severe vulnerability \cite{oe-eresolve-vuln} that has since been mitigated.
This has further influenced our decision of keeping these stacks separate although arguments can be made for both sides.

\subsubsection{Symbols}
Unlike the Intel \gls{sdk} and Rust \gls{edp}, the unified \gls{asl}  does not rely on the presence of any absolute symbols in the enclaved binary.
Because of this, the layer is able to remain completely oblivious of the symbol relocation process.
does not need to be aware of when symbols are relocated.

\subsubsection{\Glsxtrlong{tls}}

Saving variables on the execution stack is preferred over the \gls{tls} in the design of the unified \gls{asl}.
As a result, only space for a single integer-sized variable needs to be reserved in the \gls{tls} for the \gls{abi} layer to be functional.
The unified \gls{asl} depends on this variable to be initialized to zero.
This stands in sharp contrast to the Rust \gls{edp} runtime, which makes heavily use of the \gls{tls} section.
It also depends on correct initialization of some \gls{tls} located variables when the enclave is constructed.

\subsection{Sanitization and cleansing}
\label{sec:unified-sanitization-and-cleansing}

Because previous research \cite{vanbulck2019tale, alder_faulty_2020} shows that many shielding runtimes have suffered from improper sanitization or cleansing, the \textit{Sanitize/cleanse eagerly} and \textit{Sanitize early, cleanse late} principles defined in \S \ref{sec:security-oriented-design-principles} where closely followed.
This is illustrated in figure \ref{fig:unified-asl-entry-flowchart} which shows all sanitization steps being executed immediately after entry.
The only exception to this is the sanitization of the Alignment Check flag.
As seen in \S \ref{subsec:low-level-state-sanitization}, it is not possible to sanitize this flag without making use of the stack.
This sanitization step is therefore delayed until the trusted stack can be used.

All status flags, the Direction flag, Alignment Check flag and non-parameter general purpose registers are sanitized on each entry and cleansed before each exit. 
As discussed in \S \ref{sec:prototype-limitations}, the current version of the prototype does not yet sanitize the extended feature state components.
We believe it is possible to do so with low developer effort if done in a similar manner as the Rust \gls{edp}.
This however would mean that the unified \gls{asl} is only usable on systems supporting the XSAVE feature set.
More thorough solutions, comparable to the one implemented in the Intel \gls{sdk}, may be possible.
These are however not investigated in this work.

\subsection{Simulation support}

The unified \gls{asl} has support for integration with a runtime specific \gls{sgx} simulator.
This works by replacing the \gls{sgx} instruction with code that calls a function instead.
The layer can be configured to point to a specific function of the runtime simulator.
To this function, all relevant register contents are passed as function arguments.
The function is then free to simulate the operation of the \gls{sgx} hardware instruction.
We have verified that our prototype works as expected inside the Intel \gls{sdk} simulation mode.

\subsection{Configuration}

The unified \gls{asl} is configurable by changing a list of definitions that are read by the GCC preprocessor \cite{gcc-cpp-docs}.
These definitions control various offsets, function symbol names or can be used to disable certain features.
By running the GCC preprocessor with the desired configuration definitions, a shielding runtime is able to create a source file that matches its needs before compilation. 
This source file can then be added to the runtimes code base or be compiled separately and linked inside the enclave.

\subsection{Runtime integration}
\label{sec:unified-runtime-integration}

Some steps need to be taken to fully integrate the unified \gls{asl} into an enclave shielding runtime.
These steps are similar to the integration steps of other trusted \gls{abi} layer implementations.
Because the unified \gls{asl} has relatively few dependencies on the construction and initialization of an enclave (cf. \S \ref{sec:construction-initialization-dependencies}), integration effort should be lower when compared to other the other implementations seen in this work.

\subsubsection{Enclave construction}
During construction of the enclave, memory space should be reserved for a main trusted execution stack and an exception stack if exception resolvement is enabled.
The offsets to these stacks (relative to the \gls{tcs} structure) need to be configured in the configuration file of the unified \gls{asl}.
In the \gls{tls}, 8 bytes of memory need to be reserved for the unified \gls{asl}.
An offset (relative to the start of the \gls{tls}) should be configured in the configuration file, the contents should be initialized to zero.

\subsubsection{Enclave signing}
Before the enclave is signed, the \gls{tcs} structures need to be populated with the correct entry address, pointing to the entry function inside the unified \gls{asl}.

\subsubsection{Untrusted runtime interaction}
The runtime residing on the untrusted side needs to have an untrusted \gls{abi} layer that saves/restores callee-saved state and stores/fetches arguments from the right registers.

\subsubsection{Trusted runtime interaction}
On the trusted side, the runtime needs to create three functions (or two if exception resolvement is disabled) which can be branched to for each entry situation. 
The label names of these functions should be configured in the configuration file.


\section{Prototype evaluation}
\label{sec:prototype-evaluation}

This section will evaluate the finished prototype in accordance with the functional requirements and security principles that have been defined in \S \ref{sec:requirements-design-philosophy}.

\subsection{Functional evaluation}

\subsubsection{System V calling convention}
The first functional requirement stated that the prototype should respect the System V calling convention in order to be usable from the \gls{api} layer without need for any compatibility layer.
Our prototype does adhere to the full System V calling convention on the trusted side.
As mentioned in \S \ref{sec:unified-calling-convention}, the deliberate choice has been made to exclude any code from that would maintain this convention to the untrusted side from the unified \gls{asl} in order to minimize functional complexity.
This has the consequence that any runtime using this prototype as their trusted \gls{abi} layer, would still need to make sure that the calling convention is maintained at the untrusted side by implementing this logic in their untrusted \gls{abi} layer.

\subsubsection{Separation of the \gls{abi} and \gls{api} layers}
In the unified \gls{asl}, the trusted \gls{abi} layer no longer shares any variables with the \gls{api} layer.
This stands in strong contrast with the \gls{abi} of the Intel \gls{sdk} where both the \gls{api} and \gls{abi} often mutate the same memory regions.
The unified \gls{asl} also does not rely on a runtime \gls{api} layer to keep track of the executed edge call operations and evolution of the trusted stack in order to make sure the \gls{abi} interface is correctly used.
Instead, it has incorporated checks that disallow operations if they can lead to corruption of the trusted execution stack.
In this way, the \gls{api} layer can choose to remain oblivious from previous edge calling operations.
In the unified \gls{asl}, the choice between executing a nested \gls{ecall} or \gls{oret} has been given to the trusted \gls{api} layer in favor of the untrusted side as discussed in more detail in \S \ref{sec:unified-edge-calling-interface}.
The trusted \gls{api} layer still has the option to keep track of previous edge calling operations itself and use this information to implement complex policies to decide between a nested \gls{ecall} or \gls{oret}.
Because each operation is either triggered by the trusted \gls{api} layer or branches to a separate function in this \gls{api} layer, it has all the information it needs to maintain keep track of \gls{abi} operation if it wants to.
It is however not required to do so, as it can simply listen to wishes of the untrusted side (doing a nested \gls{ecall} or an \gls{oret}) as the unified \gls{asl} will return with an error if the operation is illegal in the current context.
Because of this, the unified \gls{asl} does not rely on correct behavior of an \gls{api} layer in either the mutation of shared state (as no variables are shared between the two) or correct use of it's interface (as the \gls{abi} layer incorporates it's own integrity checks).


\subsubsection{Runtime integration}
Section \ref{sec:unified-runtime-integration} discussed the various steps that a new enclave shielding runtime should take to include the unified \gls{asl}.
To give some indication of how complex integration of this prototype in real enclave shielding runtimes would be, we integrated the prototype in the Intel \gls{sdk} and Rust \gls{edp}.
These runtimes already have a trusted \gls{abi} layer and have built their codebase to interact with the specific behavior of their own \gls{abi} implementation.
By applying a few patches to both runtimes, their \gls{api} layers can be changed to use the unified \gls{asl} as a trusted \gls{abi} layer instead.
These patches are enough to have the Rust \gls{edp} runtime fully functional with the prototype.
The Intel \gls{sdk} makes use of untrusted stack allocation from inside the enclave which is not supported in the unified \gls{asl}.
%The Intel \gls{sdk} makes use of untrusted stack allocation from inside the enclave (cf. \S \ref{sec:untrusted-stack-allocation}) which is not supported in the unified \gls{asl}.
Theoretically, this could be patched by rewriting the \gls{api} code to do an \gls{ocall} that reserves this buffer from outside the enclave.
This patch is not included here and therefore the integration with the Intel \gls{sdk} suffers from a side effect where some memory buffers may be overwritten after an \gls{ocall}.

\begin{table}[ht]
    \centering
    \begin{tabular}{ |l|cc| }
        \hline
        \textbf{Patch}  & \textbf{Intel \gls{sdk}} & \textbf{Rust \gls{edp}} \\
        \hline
        \rowcolor{gray!10} \multicolumn{3}{|c|}{Untrusted Runtime / External Tools} \\
        \hline
        Replace enclave entrypoint  & 8   & 7  \\
        Untrusted \gls{abi} layer   & 7   & 33 \\
        Simulation mode             & 10  & -  \\
        \hline
        Total                       & 25  & 40 \\
        \hline
        \rowcolor{gray!10} \multicolumn{3}{|c|}{Trusted Runtime} \\
        \hline
        Code rearrangement                  & 89  & 35 \\
        Uncoupling \gls{abi} and \gls{api}  & 13  & 15 \\
        \hline
        Total                               & 102 & 50 \\
        \hline
    \end{tabular}
    \caption{Overview of the patches needed to integrate the unified \gls{asl} into the Intel \gls{sdk} and Rust \gls{edp} runtimes. The second and third column denote the amount of lines changed for that patch.}
    \label{table:integration-comparison}
\end{table}

Table \ref{table:integration-comparison} shows no significantly large code changes are needed to replace the current trusted \gls{abi} implementation in both runtimes with our prototype.
New logic was only introduced in the patches for the untrusted \gls{abi} layer and for uncoupling the \gls{abi} and \gls{api} layers in the trusted runtime.
We do note however that the runtime code has been touched here as little as possible.
If the prototype would be permanently integrated, now redundant code would be removed and \gls{api} code could potentially be rewritten to interact more logically with the new \gls{abi} interface.

The patch sizes represented in table \ref{table:integration-comparison} suggest that no large structural changes for integration in the Rust \gls{edp} is needed.
For the Intel \gls{sdk} this is at most a tentative indication as the patch size would still increase to resolve the issue with in-enclave untrusted stack allocation.
However, as this is a very specific feature that was built in to the Intel \gls{sdk} \gls{abi}, this extra integration obstacle will presumably not be present in most enclave shielding runtimes.

\subsubsection{Configurability}
To unified \gls{asl} prototype has 6 to 9 configuration options, depending on the functionality that is enabled or disabled.
Two of these options are binary flags to disable or enable functionality and four options are dedicated to telling the \gls{abi} layer which functions it can branch to.
The remaining three options are configurable offsets to specify the location of the trusted stacks and an \gls{abi} reserved variable in the \gls{tls}.
The amount of configuration options on its own is a weak indication to the extent of actual code changes due to these configuration options.
We therefore also consider the amount of conditional constructs that are being processed by the preprocessor as to indicate how heavily these options are used to generate different code.
This amounts to a total of two conditional blocks, corresponding to the binary flags that disable or enable exception resolvement and simulation support.
Code reduction, as a result to configuration by the preprocessor, amounts to \%8.6 when all features are turned off; or to \%2.4 when all features are turned on as measured by the \textit{cloc} tool \cite{cloc-github}.

\subsubsection{Runtime specific code}

The unified \gls{asl} is built entirely without any runtime specific code.
This means that this prototype is able to correctly fulfill the various trusted \gls{abi} layer responsibilities, that where covered in chapter \ref{chap:ABI-analysis}, for at least two very different enclave shielding runtimes in a general, unified manner.
This however does not mean that each runtime would incorporate the exact same code as the prototype remains configurable.
The amount of configuration is however minimal and done in a generalized way regarding the general needs of the runtime (enabling or disabling of features), the enclave memory layout (location of trusted stacks and \gls{tls} variable offset) and general control flow (name of functions that can be branched to).
We believe the absence of \gls{abi}-embedded code that needs to assure interoperability with either the Intel \gls{sdk} or Rust \gls{edp} runtimes in this prototype, serves as a strong indication towards the feasibility of unifying the \gls{abi} layer.

\subsection{Security evaluation}

In this section, the security oriented design principles that are defined in \S \ref{sec:security-oriented-design-principles} are revisited.
In the proceeding text, the functioning prototype of the unified \gls{asl} is evaluated against these design principles.

\subsubsection{Sanitize/cleanse eagerly}

As can be seen from table \ref{table:sanitization-cleansing-scope-comparison}, the unified \gls{asl} prototype comes out favorable when comparing the scope of low level state components that are sanitized/cleansed with the implementations of the Intel \gls{sdk} and Rust \gls{edp}.

\begin{table}[ht]
    \centering
    \begin{tabular}{ |l|ccc| }
        \hline
        \textbf{State Component}  & \textbf{Intel \gls{sdk}} & \textbf{Rust \gls{edp}} & \textbf{unified \gls{asl}} \\
        \hline
        \rowcolor{gray!10} \multicolumn{4}{|c|}{Sanitization} \\
        \hline
        General Purpose Registers & \checkmark &            & \checkmark \\
        Status RFLAGS             & \checkmark &            & \checkmark \\
        Direction Flag            & \checkmark & \checkmark & \checkmark \\
        Alignment Check Flag      & \checkmark & \checkmark & \checkmark \\
        Extended Feature State    & \checkmark & \checkmark \tablefootnote{\label{note:ext-feature-state}Assumes presence of the XSAVE feature set. Previously discussed in \S \ref{sec:abi-tier-sanitization-ext-feature-state}, \S \ref{sec:prototype-limitations} and \S \ref{sec:unified-sanitization-and-cleansing}.} & \checkmark \textsuperscript{\ref{note:ext-feature-state}}\\
        \hline
        \rowcolor{gray!10} \multicolumn{4}{|c|}{Cleansing} \\
        \hline
        General Purpose Registers & \checkmark & \checkmark & \checkmark \\
        Status RFLAGS             & \checkmark & \checkmark & \checkmark \\
        Direction Flag            & \checkmark & \checkmark & \checkmark \\
        Alignment Check Flag      &            & \checkmark & \checkmark \\
        Extended Feature State    & \checkmark & \checkmark \textsuperscript{\ref{note:ext-feature-state}} & \checkmark \textsuperscript{\ref{note:ext-feature-state}} \\
        \hline
    \end{tabular}
    \caption{Comparison of the sanitization/cleansing scope in the unified \gls{asl}, Intel \gls{sdk} and Rust \gls{edp}.}
    \label{table:sanitization-cleansing-scope-comparison}
\end{table}

\subsubsection{Sanitize early, cleanse late}

Table \ref{table:sanitization-cleansing-time-comparison} compares how many instructions are executed before sanitization or after cleansing of each state component.
Notably, the unified \gls{asl} is never significantly outperformed by either runtimes except for cleansing of the Alignment Check flag.
This can be explained by a different design choice between the Rust \gls{edp} implementation and the prototype.
In the Rust \gls{edp} trusted \gls{abi} layer, state cleansing is done before switching back to the untrusted stack.
Because of this, Rust \gls{edp} can sanitize the Alignment Check flag after all general purpose registers.
As the unified \gls{asl} prototype sanitizes all state except the Alignment Check flag (which needs to use the stack for sanitization) after the stacks have been switched, noticeably more instructions are placed after this sanitization step.

\begin{table}[ht]
    \centering
    \begin{tabular}{ |l|ccc| }
        \hline
        \textbf{State Component}  & \textbf{Intel \gls{sdk}} & \textbf{Rust \gls{edp}} & \textbf{unified \gls{asl}} \\
        \hline
        \rowcolor{gray!10} \multicolumn{4}{|c|}{Sanitization} \\
        \hline
        General Purpose Registers & 11 \tablefootnote{The register containing the untrusted return address is not sanitized immediately because this value first needs to be saved. After the value is saved, the register is not sanitized either until the value is ultimately implicitly overwritten. This untrusted, attacker controlled, value is only implicitly sanitized after 53 instructions.} & -          & 14         \\
        Status RFLAGS             & 2          & -          & 2          \\
        Direction Flag            & 3          & 30         & 3          \\
        Alignment Check Flag      & 28         & 30         & 33         \\
        Extended Feature State    & 46         & 17         & 8          \\
        \hline
        \rowcolor{gray!10} \multicolumn{4}{|c|}{Cleansing} \\
        \hline
        General Purpose Registers & 4          & 19         & 4          \\
        Status RFLAGS             & 3          & 11         & 2          \\
        Direction Flag            & 2          & 11         & 3          \\
        Alignment Check Flag      & -          & 11         & 21         \\
        Extended Feature State    & 64         & 14         & 14         \\
        \hline
    \end{tabular}
    \caption{Comparison of the sanitization/cleansing order in the trusted \gls{abi} layers of the Intel \gls{sdk}, Rust \gls{edp} and the unified \gls{asl}. For sanitization, the amount of instructions before that state component is fully sanitized are counted. For cleansing, the amount of instruction that are still executed after cleansing that state component are counted.}
    \label{table:sanitization-cleansing-time-comparison}
\end{table}

\subsubsection{Minimize cyclomatic complexity}

By inspecting the control flow graphs of the unified \gls{asl}, the Intel \gls{sdk} and Rust \gls{edp}, we were able to count the total amount linearly independent execution paths of their respective trusted \gls{abi} layer implementations.
For the Intel \gls{sdk} and Rust \gls{edp} runtimes we measured this to be 11 and 10 respectively.
The unified \gls{asl} prototype has a total of 8 linearly independent execution paths.
This number could slightly increase if a more complex strategy was implemented to sanitize extended feature state for systems that do not support the XSAVE feature set, as was done in the Intel \gls{sdk} implementation.
The similarity in cyclomatic complexity suggests that the unification of \gls{abi} layers does not necessarily lead to an increase of conditional branching.

\subsubsection{Minimize functional complexity}

To give some indication to the functional complexity that is present in the prototype, the total amount of lines of code in the source code is compared with the implementations of the Intel \gls{sdk} and Rust \gls{edp}.
The following measures are again provided by the cloc tool \cite{cloc-github}.
The Intel \gls{sdk} measures at a total of 591 lines of code in their \gls{abi} implementation.
This is considerably larger than the Rust \gls{edp} \gls{abi} that counts 261 lines of code.
The unified \gls{asl} prototype comes in most favorable with 219 lines of source code.

%\subsubsection{Avoid mutable \gls{eresolve} dependencies}
%The unified \gls{asl} does not make use of any mutable \gls{eresolve} dependency.

% TODO: add unification conclusion
