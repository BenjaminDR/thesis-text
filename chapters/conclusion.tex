\chapter{Conclusion}
\label{chap:conclusion}

% Here conclusionary introduction

% Small outline of this chapter

\section{Contributions}

This master's thesis explored the feasibility of unifying software responsibilities that secure enclaved binaries at the level of the \gls{abi} in a single implementation.
It has done so by carefully analyzing current measures that are realized in existing implementations.
Subsequently a prototype was engineered that illustrated the feasibility of unifying this software component by successful integration in two enclave shielding runtimes.
More specifically, the following contributions were made.

By reviewing past research and analyzing the runtime source code of the Intel \gls{sdk} and Fortanix \gls{edp} runtimes, various known \gls{abi} shielding responsibilities were categorically documented in chapter \ref{chap:ABI-analysis}.
For each shielding responsibility, their presence in the Intel \gls{sdk} \cite{intel-sgx-sdk, intel-sdk-github} and Rust \gls{edp} \cite{rust-edp-github} runtimes were noted and implementational differences were highlighted.
On a more functional note, the compliance of both runtimes with the System V \gls{abi} \cite{System-V-ABI} was highlighted.
During this analysis, special attention was dedicated to the low-level assembly code that was integrated in both runtimes.
The inner workings of these assembly stubs were dissected at multiple levels of detail as to provide a clear insight in their design.
As a running example, the low-level assembly code was presented in Section \ref{sec:interface-implementation}.
Visualizations regarding the analysis of other assembly code sections can be found in Appendices \ref{app:trusted-abi-layer-flowcharts-Intel} and \ref{app:trusted-abi-layer-flowcharts-Rust}.

Moving on from current implementations, a series of functional requirements are formed in \S \ref{sec:functional-requirements-unification} to be used as an indicative measure to the feasibility of using a trusted \gls{abi} layer in a unified setting.
In addition, a series of security oriented design principles are defined in \S \ref{sec:security-oriented-design-principles} based on the landscape of previous \gls{abi} related vulnerabilities in \gls{sgx} compatible shielding runtimes.

A further contribution was the creation of a functional trusted \gls{abi} layer that could be integrated in both the Intel \gls{sdk} and Rust \gls{edp} runtimes.
Various design and implementation related decisions were discussed in \S \ref{sec:design-and-implementation}, the limitations of the resulting prototype were highlighted in \S \ref{sec:prototype-limitations}.
Finally, an evaluation of this prototype was performed based on the previously defined requirements and principles in \S \ref{sec:prototype-evaluation}.
This evaluation ties the practical work done in this research project back to our original research question.
It does so by giving indicative measures about the feasibility of unification; and an insight to the resilience of this implementation to current and possibly future attack vectors.


\section{Limitations and Challenges}

This section covers the limitations and challenges that were faced in the process of this research project.

The brief time span of a master's thesis was a restricting factor that allowed a thorough analysis of only two enclave shielding runtimes.
These runtimes were chosen because of their past influence on the Intel \gls{sgx} software ecosystem, because they target different high-level programming languages and because of the contrast in the software abstraction they have built.
Two runtimes do not, however, provide a complete perspective on the \gls{abi} related requirements that may be imposed by enclave shielding runtimes.
Because of this limitation, certain design decisions in the created prototype might be revisited with future research.
In the creation of the prototype for a unified trusted \gls{abi} layer, this became apparent in two cases.
It was unclear whether the additional complexity cost of in-enclave untrusted stack allocation was justified, as the general benefit of this extra feature was not known across enclave shielding runtimes besides the Intel \gls{sdk} (cf. \ref{sec:limitations-untrusted-stack-allocation}).
Similarly, while defining the unified edge calling interface, the choice was made to only allow the \gls{oret} operation to be invoked from inside the trusted enclave. 
Without loss of functionality, this provided the trusted \gls{api} layer with more flexibility at the cost of a less straightforward edge calling interface.
We found that this choice worked well within the scope of the runtimes that were analyzed in this work and has the highest probability of generalizing well to all shielding runtimes.
Still, it is entirely possible that this design choice was unnecessary in which case \gls{oret} operations can alternatively be handled transparently by the trusted \gls{abi} layer.
It is therefore important to recognize that this work exclusively \textit{indicates} towards the possibility and feasibility of unifying the trusted \gls{abi} layer in Intel \gls{sgx} shielding runtimes.

Another limitation of this work lies in the prototype evaluation.
The prototype was functionally evaluated to provide an insight in the feasibility of using this trusted \gls{abi} layer for multiple different shielding runtimes.
There is however no direct objective measure to do this.
The functional evaluation is therefore directly based on the requirements that were defined in this work and therefore their relevance can be argued.
A similar approach was taken for the security evaluation where the prototype was evaluated in regard to security principles that were only defined in this work.
Although these principles are defined to reflect previously discovered \gls{abi} layer vulnerabilities, following them might not gain a concrete security improvement.
The evaluation also does not reflect the runtime performance of the \gls{abi} layer.
No direct indication is found that would suggest a significant performance diversion from the Intel \gls{sdk} and Rust \gls{edp} implementations.
This claim remains tentative without an actual measurement that substantiates it.

One major challenge that was encountered in the process of this research endeavor was patching the Intel \gls{sdk} and Rust \gls{edp} runtimes.
These open-source projects contain hardly any developer targeted public documentation.
This forced us to study a large fraction of their source code, compile and linking options before establishing the required knowledge to do small changes.
The lack of documentation was especially disadvantageous when building the trusted component of the Rust \gls{edp} runtime.
This trusted component is embedded in the Rust compiler \cite{rust-lang-github}, organized in a separate compile target.
Although the bootstrapping process of this compiler by itself is already complicated, it is generally well documented.
No documentation was found to indicate that the x86\_64-fortanix-unknown-sgx target requires special intervention to build correctly.
As the build process of the compiler succeeds with normal configuration, pinpointing the issue was not trivial.
By combing through closed issues in the rust repository and carefully inspecting all build components, the discovery was made that the Fortanix target uses a separate fork of the llvm compiler \cite{llvm-github} that customizes the \textit{libunwind} functionality in the \textit{lld} linker.
This provided the necessary knowledge that the compiler building process needed to be configured to block pre-built  llvm usage, build the llvm compiler from source and use the \textit{lld} linker.
By inspecting \gls{ci} scripts, specific \textit{clang} versions where discovered that needed to be used to compile the Fortanix target.


\section{Future work}

Because of the limited scope of studied runtimes in this work, the breadth of desired \gls{abi} layer functionality by enclave shielding runtimes may be incomplete.
Future work may be able to provide a more definitive answer on the various \gls{abi} related needs that are required to build current runtime software abstractions by analyzing other \gls{abi} implementations.
Current examples of runtimes that could provide additional insight in the relevance and feasibility of this research endeavor include OpenEnclave \cite{oe-sdk, oe-sdk-github}, Graphene \cite{graphene} and Enarx \cite{enarx-web}.

It may also be worthwhile to investigate how the unification attempt presented in this work would translate to other \gls{sgx} compatible systems. 
Because of the backwards compatible nature of the x86 architecture, \gls{sgx} is technically also available for systems that do not support the XSAVE feature set or implement the 32bit version of the x86 architecture.
\gls{sgx} is also available on the widely used Windows operating system which uses a different calling convention.

One of the bigger roadblocks for a unified \gls{abi} shielding layer may be the variety that originates from different binary memory layouts that are loaded into the enclave.
In a parallel line of research, it may be advantageous to explore the feasibility of a unified binary structure that can be loaded into enclaves.
This binary structure can be derived from the standard \gls{elf} file format to include stack, heap and \gls{tls} structures that are easily locatable and usable from inside an enclave.
Less security relevant advantages of a unified binary structure for enclaves could include more unified enclave construction and loading logic.
On the trusted side there could also be benefits, as the trusted \gls{abi} layer would rely less on configuration and would be able to include binary related initialization logic like symbol relocation.

Another point of focus could be laid on the more feature complete software interface abstraction that is commonly build in the trusted \gls{api} layer of enclave shielding runtimes.
The wide vulnerability landscape that was discovered in previous research \cite{vanbulck2019tale} may indicate that a robust unified design can prove to be beneficial to support a variety of different enclave shielding runtimes.
This line of research could potentially make use of the \gls{abi} edge calling interface that was implemented in this work or by an established enclave shielding runtime.
With this \gls{abi} building block, a powerful procedure-like software abstraction can be created.
Alternatively, a more simple syscall-like software abstraction can be built to support library-os shielding runtimes. 
It may prove interesting to provide additional insight in if/how the needs of these software abstractions differ.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "thesis"
%%% End: 
